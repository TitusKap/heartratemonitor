
struct Config
{
	uint32_t comPort;
	uint32_t baudRate;
};

bool
ReadConfig(Config * cfg, const char * filename)
{
	FILE * file = fopen(filename, "r");
	if (!file)
	{
		return false;
	}

	fscanf(file, "%u %u", &cfg->comPort, &cfg->baudRate);
	fclose(file);

	return true;
}

bool
WriteConfig(Config * cfg, const char * filename)
{
	FILE * file = fopen(filename, "w");
	if (!file)
	{
		return false;
	}

	fprintf(file, "%u %u", cfg->comPort, cfg->baudRate);
	fclose(file);

	return true;
}

