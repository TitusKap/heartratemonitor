
struct RenderData
{
	HDC dc;
	RECT canvasRect;

	HBRUSH brush;
	HPEN pen;
	HPEN hintPen;

	HFONT bpmFont;
	HFONT hintFont;

	//RECT rWindow;
	int radius;
	int margin;
	RECT rValue;
	RECT rBpm;
	int bpmPadding;
	RECT rHistory;
};

void
Draw(RenderData * rd, PulseData * pd)
{
	SelectObject(rd->dc, rd->brush);
	SelectObject(rd->dc, rd->pen);

	// NOTE(titus): Draw boxes for elements
	RoundRect(rd->dc, rd->rValue.left, rd->rValue.top, rd->rValue.right, 
		rd->rValue.bottom, rd->radius, rd->radius);
	RoundRect(rd->dc, rd->rBpm.left, rd->rBpm.top, rd->rBpm.right,
		rd->rBpm.bottom, rd->radius, rd->radius);
	RoundRect(rd->dc, rd->rHistory.left, rd->rHistory.top, rd->rHistory.right,
		rd->rHistory.bottom, rd->radius, rd->radius);

	// NOTE(titus): Draw bpm text
	{
		if (!rd->bpmFont)
		{
			int widgetHeight = rd->rBpm.bottom - rd->rBpm.top;
			rd->bpmFont = CreateFontA(
				widgetHeight - 2 * rd->bpmPadding,
				0,
				0,
				0,
				FW_BOLD,
				FALSE,
				FALSE,
				FALSE,
				ANSI_CHARSET,
				OUT_DEFAULT_PRECIS,
				CLIP_DEFAULT_PRECIS,
				CLEARTYPE_QUALITY,
				DEFAULT_PITCH || FF_MODERN,
				"");

			ASSERT(rd->bpmFont);
		}

		char text[8];
		int textLength = sprintf_s(text, "%d", GetLastBpm(pd));

		SelectObject(rd->dc, rd->bpmFont);

		SIZE textSize;
		GetTextExtentPoint32(
			rd->dc,
			text,
			textLength,
			&textSize);

		const int bpmWidth = rd->rBpm.right - rd->rBpm.left;
		int x = rd->rBpm.left + (bpmWidth - textSize.cx) / 2;
		TextOut(rd->dc, x, rd->bpmPadding, text, textLength);
	}
	
	// NOTE(titus): Draw raw pulse values
	{
		const uint32_t valueWidth = rd->rValue.right - rd->rValue.left;
		const int count = (valueWidth > pd->valueSize) ? pd->valueSize : valueWidth;
		const int start = (pd->valueStart-count+pd->valueSize) % pd->valueSize; 

		const float yScale = (rd->rValue.bottom - rd->rValue.top) / -1000.f;
		int val = pd->valueBuffer[start];
		float y = rd->rValue.bottom + val * yScale;
		MoveToEx(rd->dc, rd->rValue.left, (int)y, nullptr);

		for (int i = 1; i < count; ++i)
		{
			const int idx = (start + i) % pd->valueSize;
			val = pd->valueBuffer[idx];
			y = rd->rValue.bottom + val * yScale;
			LineTo(rd->dc, rd->rValue.left + i, (int)y);
		}
	}

	// NOTE(titus): Draw bpm history
	{
		const int xOffset = 20; // Offset to make space for guide line labels
		const uint32_t historyWidth = rd->rHistory.right - rd->rHistory.left - xOffset;

		int count = (historyWidth > pd->bpmSize) ? pd->bpmSize : historyWidth;
		int start = pd->bpmStart-count+pd->bpmSize;

		for (uint32_t i = 0; i < pd->bpmSize; ++i)
		{
			const int idx = (start + i) % pd->bpmSize;
			int val = pd->bpmBuffer[idx];
			if (val != 0)
			{
				start = (start + i) % pd->bpmSize;
				count = (count - i);
				break;
			}
		}

		start = start % pd->bpmSize;

		// NOTE(titus): loop over all bpm values to get upper and lower
		// limits
		int max = 0;
		int min = INT32_MAX;
		for (int i = 0; i < count; ++i)
		{
			const int idx = (start + i) % pd->bpmSize;
			int val = pd->bpmBuffer[idx];
			if (val > max) max = val;
			if (val < min) min = val;
		}

		min = ((min+5)/10) * 10 - 5;
		max = ((max+5)/10) * 10 + 5;

		if (min == max)
			++max;

		const int yOffset = -min;
		const float yScale = (float)(rd->rHistory.bottom - rd->rHistory.top) / -(max - min);

		int minGuide = ((min/10)+1)*10;
		int maxGuide = (max/10)*10;
		SelectObject(rd->dc, rd->hintFont);

		// NOTE(titus): Draw guide lines
		char text[8];
		for (int i = minGuide; i <= maxGuide; i+=10)
		{
			float y = rd->rHistory.bottom + (i + yOffset) * yScale;
			int x = rd->rHistory.left + xOffset;

			SelectObject(rd->dc, rd->hintPen);
			MoveToEx(rd->dc, x, (int)y, nullptr);
			LineTo(rd->dc, rd->rHistory.right-3, (int)y);

			// TODO(titus): Add label for guide line
			int textLength = sprintf_s(text, "%d", i);
			SelectObject(rd->dc, rd->pen);
			TextOut(rd->dc, rd->rHistory.left + 4, (int)y - 7, text, textLength);
		}


		int val = pd->bpmBuffer[start];
		float y = rd->rHistory.bottom + (val + yOffset) * yScale;
		int x = rd->rHistory.right - count;
		MoveToEx(rd->dc, x, (int)y, nullptr);

		for (int i = 0; i < count; ++i)
		{
			const int idx = (start + i) % pd->bpmSize;
			val = pd->bpmBuffer[idx];
			y = rd->rHistory.bottom + (val + yOffset) * yScale;
			x = rd->rHistory.right - count + i;
			LineTo(rd->dc, x, (int)y);
		}
	}
}
