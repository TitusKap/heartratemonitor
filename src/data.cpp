
struct PulseData
{
	// NOTE(titus): ringbuffer for raw sensor values
	uint16_t valueBuffer[400];
	uint32_t valueSize;
	uint32_t valueStart;

	// NOTE(titus): ringbuffer for bpm history
	uint8_t bpmBuffer[200];
	uint32_t bpmSize;
	uint32_t bpmStart;
};

void
Init(PulseData * data)
{
	data->valueSize = 400;
	data->bpmSize = 200;
}

void
AddValue(PulseData * data, uint16_t val)
{
	uint32_t idx = data->valueStart % data->valueSize;
	data->valueBuffer[idx] = val;
	data->valueStart = (data->valueStart+1) % data->valueSize;
}

void
AddBpm(PulseData * data, uint8_t bpm)
{
	uint32_t idx = data->bpmStart % data->bpmSize;
	data->bpmBuffer[idx] = bpm;
	data->bpmStart = (data->bpmStart+1) % data->bpmSize;

	// Update bpm text file
	HANDLE file = CreateFileA(
		"bpm.txt",
		GENERIC_WRITE,
		FILE_SHARE_READ,
		nullptr,
		CREATE_ALWAYS,
		FILE_ATTRIBUTE_NORMAL,
		nullptr);
	if (file != INVALID_HANDLE_VALUE)
	{
		char buffer[8];
		int count = snprintf(buffer, 8, "%u", bpm);
		WriteFile(file, buffer, count, nullptr, nullptr);

		CloseHandle(file);
	}
}

uint8_t
GetLastBpm(const PulseData * data)
{
	uint32_t idx = (data->bpmStart + data->bpmSize - 1) % data->bpmSize;
	return data->bpmBuffer[idx];
}
