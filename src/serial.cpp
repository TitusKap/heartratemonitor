
struct ComHandle
{
	uint32_t comPort;
	HANDLE handle;
	
	char buffer[16];
	size_t size = sizeof(buffer);
	char * start = &buffer[0];
};

void
ConvertComPort(char * portName, uint32_t port)
{
	if (port < 10)
	{
		sprintf(portName, "COM%u", port);
	}
	else
	{
		sprintf(portName, "\\\\.\\COM%u", port);
	}
}

void
ConvertComPort(uint32_t * port, const char * portName)
{
	for (; *portName != '\0'; ++portName)
	{
		if (isdigit(*portName))
		{
			break;
		}
	}
	sscanf(portName, "%u", port);
	return;
}


void
SetComPort(ComHandle * com, uint32_t comPort, uint32_t baudRate)
{
	if (comPort == com->comPort)
		return;

	com->comPort = comPort;

	if (com->handle)
	{
		CloseHandle(com->handle);
	}

	TCHAR name[10];
	ConvertComPort(name, comPort);

	com->handle = CreateFile(
		name,
		GENERIC_READ,
		0,
		nullptr,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL,
		nullptr);

	if (com->handle == INVALID_HANDLE_VALUE)
	{
		com->handle = nullptr;
		com->comPort = 0;
		return;
	}

	DCB dcb = {};
	dcb.DCBlength = sizeof(dcb);
	dcb.BaudRate = baudRate;

	SetCommState(com->handle, &dcb);

	COMMTIMEOUTS timeouts;
	timeouts.ReadIntervalTimeout = MAXDWORD;
	timeouts.ReadTotalTimeoutMultiplier = 0;
	timeouts.ReadTotalTimeoutConstant = 0;
	timeouts.WriteTotalTimeoutMultiplier = 0;
	timeouts.WriteTotalTimeoutConstant = 0;

	// TODO(titus): error handling
	SetCommTimeouts(com->handle, &timeouts);
}

static
char *
ReadFromBuffer(ComHandle * com)
{
	char * ch;

	ch = com->start;
	while (*ch != '\0' && *ch != '\n')
		++ch;

	if (*ch == '\n')
	{
		ASSERT(*(ch-1) == '\r');
		*(ch-1) = '\0';

		char * result = com->start;
		com->start = ch+1;
		return result;
	}
	
	return nullptr;
}

char *
ReadLine(ComHandle * com)
{
	char * ch;
	char * dst;

	if (!com->handle)
		return nullptr;

	// NOTE(titus): Read line from existing buffer
	if (char * line = ReadFromBuffer(com))
		return line;

	// NOTE(titus): copy remaining buffer to start
	ch = com->start;
	dst = com->buffer;
	com->start = com->buffer;
	while (*ch != '\0')
		*(dst++) = *(ch++);

	// NOTE(titus): try to read new data
	size_t remaining = com->size - (dst - com->start) - 1;
	DWORD read;
	ReadFile(
		com->handle,
		dst,
		(DWORD)remaining,
		&read,
		nullptr);
	dst[read] = '\0';

	return ReadFromBuffer(com);
}
