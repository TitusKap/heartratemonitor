
#define _CRT_SECURE_NO_WARNINGS

#include <stdint.h>
#include <stdio.h>

#include <windows.h>
#include <CommCtrl.h>

#define ASSERT(cond) if(!(cond)){int*np=0;*np=0;}

#include "data.cpp"
#include "serial.cpp"
#include "draw.cpp"
#include "config.cpp"

const char * configFileName = "heartratemonitor.config";

PulseData g_pulseData;
ComHandle g_comhandle;
RenderData g_renderData;

HWND g_main;

struct Settings
{
	HWND window;

	Config config;

	HWND comPortCB;
	HWND baudRateCB;
} g_settings;

const uint32_t baudRates[14] = {
	CBR_110, CBR_300, CBR_600, CBR_1200, CBR_2400, CBR_4800,
	CBR_9600, CBR_14400, CBR_19200, CBR_38400, CBR_57600,
	CBR_115200, CBR_128000, CBR_256000
};

LPCSTR settingsClassName = "HeartRateMonitorSettingsWindow";
LPCSTR mainClassName = "HeartRateMonitorMainWindow";

void
CreateSettingsWindow()
{
	if (g_settings.window)
	{
		return;
	}

	DWORD style = WS_CAPTION | WS_POPUP | WS_SYSMENU;
	DWORD exStyle = WS_EX_DLGMODALFRAME;

	g_settings.window = CreateWindowEx(
		exStyle, settingsClassName,
		"Heart Rate Monitor | Settings", style,
		CW_USEDEFAULT, CW_USEDEFAULT,
		400, 124,
		nullptr, nullptr,
		GetModuleHandle(nullptr), nullptr);
	if (!g_settings.window)
	{
		FatalAppExit(0, "Failed to create window");
	}

	RECT wndRect;
	GetClientRect(g_settings.window, &wndRect);

	HWND serialGroup = CreateWindowEx(
		0, "BUTTON",
		"Select Port", BS_GROUPBOX | WS_CHILD | WS_VISIBLE,
		2, 2,
		wndRect.right-4, 80,
		g_settings.window, nullptr,
		nullptr, nullptr);
	if (!serialGroup)
	{
		FatalAppExit(0, "Failed to create comBox");
	}

	RECT serialRect;
	GetClientRect(serialGroup, &serialRect);

	CreateWindowEx(
		0, WC_STATIC,
		"COM Device", WS_CHILD | WS_VISIBLE | WS_OVERLAPPED,
		4, 20,
		serialRect.right-8, 40,
		g_settings.window, nullptr,
		nullptr, nullptr);

	g_settings.comPortCB = CreateWindowEx(
		0, WC_COMBOBOX,
		"", CBS_DROPDOWNLIST | CBS_HASSTRINGS | WS_VSCROLL | WS_CHILD | WS_VISIBLE | WS_OVERLAPPED,
		104, 20,
		serialRect.right-118, 120,
		g_settings.window, nullptr,
		nullptr, nullptr);
	if (!g_settings.comPortCB)
	{
		FatalAppExit(0, "Failed to create comBox");
	}

	CreateWindowEx(
		0, WC_STATIC,
		"Baud Rate", WS_CHILD | WS_VISIBLE | WS_OVERLAPPED,
		4, 50,
		serialRect.right-8, 20,
		g_settings.window, nullptr,
		nullptr, nullptr);

	g_settings.baudRateCB = CreateWindowEx(
		0, WC_COMBOBOX,
		"", CBS_DROPDOWNLIST | CBS_HASSTRINGS | WS_VSCROLL | WS_CHILD | WS_VISIBLE | WS_OVERLAPPED,
		104, 50,
		serialRect.right-118, 120,
		g_settings.window, nullptr,
		nullptr, nullptr);
	if (!g_settings.comPortCB)
	{
		FatalAppExit(0, "Failed to create comBox");
	}

	char baudRateStr[8];
	for (int i = 0; i < 14; ++i)
	{
		sprintf_s(baudRateStr, "%u", baudRates[i]);
		SendMessage(g_settings.baudRateCB, CB_ADDSTRING, 0, (LPARAM)baudRateStr);

		if (baudRates[i] == CBR_115200)
		{
			SendMessage(g_settings.baudRateCB, CB_SETCURSEL, i, 0);
		}
	}
}

void
OpenSettingsWindow()
{
	// NOTE(titus): Create settings window if not already existing
	CreateSettingsWindow();

	// NOTE(titus): Update COM ports list
	SendMessage(g_settings.comPortCB, (UINT)CB_RESETCONTENT, 0, 0);

	TCHAR name[10];
	TCHAR buffer[256];

	// TODO(titus): extend chack to 256 COM devices
	for (int i = 1; i < 256; ++i)
	{
		ConvertComPort(name, i);

		DWORD result = QueryDosDevice(name, buffer, 256);

		if (result != 0)
		{
			SendMessage(g_settings.comPortCB, (UINT)CB_ADDSTRING,
				0, (LPARAM)name); 
		}
	}
	
	SetWindowPos(
		g_settings.window,
		HWND_TOP,
		0, 0, 0, 0,
		SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
}

void
OnPaint(HWND window)
{
	PAINTSTRUCT ps;
	HDC dc = BeginPaint(window, &ps);
	SetTextAlign(dc, TA_TOP);

	RECT canvasRect;
	GetClientRect(window, &canvasRect);
	int width = canvasRect.right - canvasRect.left;
	int height = canvasRect.bottom - canvasRect.top;

	// Create back buffer
	HDC memDc = CreateCompatibleDC(dc);
	HBITMAP memBitmap = CreateCompatibleBitmap(dc, width, height);
	SelectObject(memDc, memBitmap);

	// clear to green
	HBRUSH greenBg = CreateSolidBrush(0x0000FF00);
	FillRect(memDc, &canvasRect, greenBg);
	DeleteObject(greenBg);

	const COLORREF bg = 0x00B4B4B4;
	SetBkColor(memDc, bg);

	if (!g_renderData.brush)
	{
		g_renderData.brush = CreateSolidBrush(bg);
	}
	if (!g_renderData.pen)
	{
		g_renderData.pen = CreatePen(PS_SOLID, 2, 0x00000000);
	}
	if (!g_renderData.hintPen)
	{
		g_renderData.hintPen = CreatePen(PS_SOLID, 2, 0x00666666);
	}
	if (!g_renderData.hintFont)
	{
		g_renderData.hintFont = CreateFontA(
			14,
			0,
			0,
			0,
			FW_BOLD,
			FALSE,
			FALSE,
			FALSE,
			ANSI_CHARSET,
			OUT_DEFAULT_PRECIS,
			CLIP_DEFAULT_PRECIS,
			CLEARTYPE_QUALITY,
			DEFAULT_PITCH || FF_MODERN,
			"");
	}

	g_renderData.dc = memDc;
	Draw(&g_renderData, &g_pulseData);

	// Blip to front and delete back buffer
	BitBlt(dc, 0, 0, width, height, memDc, 0, 0, SRCCOPY); 
	DeleteObject(memBitmap);
	DeleteDC(memDc);
	DeleteDC(dc);

	EndPaint(window, &ps);
}

LRESULT
CALLBACK
WndProc(HWND window, UINT message, WPARAM wparam, LPARAM lparam)
{
	switch (message)
	{
		case WM_PAINT:
		{
			OnPaint(window);
		} return 0;

		case WM_GETMINMAXINFO:
		{
			MINMAXINFO * mmi = (MINMAXINFO*)lparam;
			mmi->ptMinTrackSize.x = 270;
			mmi->ptMinTrackSize.y = 238;
		} return 0;

		case WM_SIZE:
		{
			const int margin = 4;

			g_renderData.margin = margin;
			g_renderData.radius = 10;
			
			LONG width = LOWORD(lparam);
			LONG height = HIWORD(lparam);
			LONG vcut = width - (132 + 16);
			LONG hcut = 100;

			g_renderData.rValue = {margin, margin, vcut, height-margin};
			g_renderData.rBpm = {vcut+margin, margin, width-margin, hcut};
			g_renderData.rHistory = {vcut+margin, hcut+margin, width-margin, height-margin};
			g_renderData.bpmPadding = 8;

			InvalidateRect(window, nullptr, FALSE);
		} return 0;
		
		case WM_KEYDOWN:
		{
			if (wparam == VK_DELETE)
			{
				memset(g_pulseData.valueBuffer, 0x00, sizeof(g_pulseData.valueBuffer));
				memset(g_pulseData.bpmBuffer, 0x00, sizeof(g_pulseData.bpmBuffer));
			}
			else if (wparam == VK_F6)
			{
				OpenSettingsWindow();
			}
		} return 0;

		case WM_TIMER:
		{
			InvalidateRect(window, nullptr, FALSE);
		} return 0;

		case WM_DESTROY:
		{
			PostQuitMessage(0);
		} return 0;
	}
	return DefWindowProc(window, message, wparam, lparam);
}

LRESULT
CALLBACK
WndProcSettings(HWND window, UINT message, WPARAM wparam, LPARAM lparam)
{
	switch (message)
	{
		case WM_DESTROY:
		{
			g_settings.window = nullptr;
		} return 0;

		case WM_PAINT:
		{
			PAINTSTRUCT ps;
			HDC hdc = BeginPaint(window, &ps);
			FillRect(hdc, &ps.rcPaint, (HBRUSH)COLOR_WINDOW);
			EndPaint(window, &ps);
		} return 0;

		case WM_COMMAND:
		{
			bool settingsChanged = false;
			if (lparam == (LPARAM)g_settings.comPortCB)
			{
				if(HIWORD(wparam) == CBN_SELCHANGE)
				{
					LRESULT idx = SendMessage((HWND)lparam, CB_GETCURSEL, 0, 0);
					TCHAR name[10];
					SendMessage((HWND)lparam, CB_GETLBTEXT, idx, (LPARAM)name);
					ConvertComPort(&g_settings.config.comPort, name);
					settingsChanged = true;
				}
			}
			else if (lparam == (LPARAM)g_settings.baudRateCB)
			{
				if(HIWORD(wparam) == CBN_SELCHANGE)
				{
					LRESULT idx = SendMessage((HWND)lparam, CB_GETCURSEL, 0, 0);
					g_settings.config.baudRate = baudRates[idx];
					settingsChanged = true;
				}
			}

			if (settingsChanged)
			{
				WriteConfig(&g_settings.config, configFileName);
				SetComPort(&g_comhandle, g_settings.config.comPort, g_settings.config.baudRate);
			}
		} return 0;
	}

	return DefWindowProc(window, message, wparam, lparam);
}

int
WINAPI
WinMain(HINSTANCE instance, HINSTANCE, LPSTR args, int showCmd)
{
	// TODO(titus): parse args
	(void)args;

	Init(&g_pulseData);

	// NOTE(titus): Create window classes
	WNDCLASSEX wcm = {};
	wcm.cbSize = sizeof(wcm);
	wcm.style = CS_OWNDC;
	wcm.hbrBackground = CreateSolidBrush(0x0000FF00);
	wcm.lpfnWndProc = WndProc;
	wcm.hInstance = instance;
	wcm.hIcon = (HICON)LoadImage(instance, "MainIcon", IMAGE_ICON, 0, 0, LR_DEFAULTCOLOR);
	wcm.hIconSm = nullptr;
	wcm.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcm.lpszClassName = mainClassName;

	ATOM windowMainClass = RegisterClassEx(&wcm);
	if (!windowMainClass)
	{
		FatalAppExit(0, "Failed to register window");
	}

	WNDCLASSEX wcs = {};
	wcs.cbSize = sizeof(wcs);
	wcs.style = CS_OWNDC;
	wcs.lpfnWndProc = WndProcSettings;
	wcs.hInstance = instance;
	wcs.hIcon = nullptr; // TODO(titus): Add Icon
	wcs.hIconSm = nullptr; // TODO(titus): Add small icon
	wcs.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcs.lpszClassName = settingsClassName;

	ATOM windowSettingsClass = RegisterClassEx(&wcs);
		if (!windowSettingsClass)
	{
		FatalAppExit(0, "Failed to register window");
	}

	// NOTE(titus): Create main display window
	DWORD style = WS_SYSMENU | WS_SIZEBOX;
	DWORD exStyle = 0;

	RECT rect = {0, 0, 300, 200};
	AdjustWindowRectEx(&rect, style, FALSE, exStyle);

	g_main = CreateWindowEx(
		exStyle, mainClassName,
		"Heart Rate Monitor", style,
		CW_USEDEFAULT, CW_USEDEFAULT,
		rect.right - rect.left, rect.bottom - rect.top,
		nullptr, nullptr,
		instance, nullptr);

	if (!g_main)
	{
		FatalAppExit(0, "Failed to create window");
	}

	ReadConfig(&g_settings.config, configFileName);
	SetComPort(&g_comhandle, g_settings.config.comPort, g_settings.config.baudRate);

	SetTimer(
		g_main,
		0,
		16, // => 60 fps
		nullptr);

	ShowWindow(g_main, showCmd);

	MSG msg;
	while (GetMessage(&msg, nullptr, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);

		while (char * line = ReadLine(&g_comhandle))
		{
			char type = line[0];

			uint32_t value;
			sscanf_s(line+1, "%u", &value);

			if (type == 'S')
			{
				ASSERT(value < UINT16_MAX);
				AddValue(&g_pulseData, (uint16_t)value);
			}
			else if (type == 'B')
			{
				ASSERT(value < UINT8_MAX);
				AddBpm(&g_pulseData, (uint8_t)value);
			}

			OutputDebugString(line);
			OutputDebugString("\n");
		}
	}
}

