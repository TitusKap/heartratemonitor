@echo off

where cl
if errorlevel 1 (
	echo cl.exe was not found, please make sure to call %~nx0 from developer console.
	pause
	goto end
)

if not exist bin\ (mkdir bin)
if not exist obj\ (mkdir obj)

set libs=User32.lib Gdi32.lib

if "%1%"=="release" (
	set flags=/O2 /WX
	echo Building for release.
) else (
	set flags=/Z7
	echo Building for development.
)

rc /nologo /r /fo obj/icon.res src\icon.rs

cl.exe /nologo %flags% /W4 src/heartratemonitor.cpp obj/icon.res %libs% /Febin\ /Foobj\

if errorlevel 1 (echo FAILED) else (echo SUCCESS)

:end