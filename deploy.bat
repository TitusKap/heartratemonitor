@echo off
set mydate=%date:~6,4%%date:~0,2%%date:~3,2%

cd %~dp0

echo Create x64 binary release
call "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvars64.bat"
call "build.bat" release
tar -acf "heartratemonitor_%mydate%_x64.zip" LICENSE README.md -C bin heartratemonitor.exe

echo Create x86 binary release
call "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvars32.bat"
call "build.bat" release
tar -acf heartratemonitor_%mydate%_x86.zip LICENSE README.md -C bin heartratemonitor.exe

echo Create source code release
tar -acf heartratemonitor_%mydate%_source.zip LICENSE README.md build.bat src img