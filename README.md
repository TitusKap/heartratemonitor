# Heart Rate Monitor

Pulse sensor visualization intended for live stream or video integration.
Intended to be used with a microcontroller and pulse sensor from [pulsesensor.com](http://pulsesensor.com/).

![Demo of the program](img/demo.gif)

## Build

To build the software yourself you need to have downloaded the source release or the repository.
Just call the `build.bat` script with a developer command line and it will create a `bin` folder to which the executable will be written.
I only ever tested the VS2019 compiler but other compilers should work as well.

## Install

The software is portable and will only require you to have write access in the working directory to save settings and write a text file containing the current bpm value for simple integrations into other applications.

## Configuration

Because this is build specifically for the sensors and software from [pulsesensor.com](http://pulsesensor.com/).
You will need their sensor and a microcontroller compatible with the arduino software.

Please follow these steps to program your microcontroller:
1. Follow the instructions on [this](https://pulsesensor.com/pages/installing-our-playground-for-pulsesensor-arduino) site and install the pulesensor libraries.
2. Afterwards you have to Set up either the PulseSensor_BPM or PulseSensor_BPM_Alternative sketch as described [here](https://pulsesensor.com/pages/getting-advanced).
3. After setting `PULSE_INPUT` to your sensor and any additional customization you want to doe, make sure to also set `const int OUTPUT_TYPE = PROCESSING_VISUALIZER;`
4. Upload the sketch to your microcontroller and attach the sensor to your body.
5. Open the heartratemonitor executable and press <kbd>F6</kbd> to open the settings menu. Select the COM device that your microcontroller is recognized at and make sure the baud-rate matches the one you set in the arduino sketch (115200 by default).

You should now see the pulse data visualized in the heartratemonitor executable.

## Keybinds

- <kbd>F6</kbd> Open settings menu
- <kbd>DEL</kbd> Clear all pulse data

## Todo

- [ ] Add Headless mode (no windows, bpm output to text file only)
- [ ] Add more customization (chroma key color, layout, borders)